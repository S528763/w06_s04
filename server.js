var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var path = require('path');


app.use(express.static(path.join(__dirname))); 

// 2. replace the inside lines of your app.get 
app.get('/', function (req, res) {       
     res.sendFile(path.join(__dirname, '../w06/assets', 'index.html'));
});

// 3. Add the following function 
io.on('connection', function(socket){ 
      socket.on('chatMessage', function(from, msg){
           io.emit('chatMessage', from, msg);  
      });
      socket.on('notifyUser', function(user){
           io.emit('notifyUser', user);  
      });
});

server.listen(8081, function () {
  console.log('listening on http://localhost:8081/');
});